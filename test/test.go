package main

import "bitbucket.org/sbrnetmarketing/go-logger/logger"
import "fmt"
import "flag"
import "time"

func main() {
	logLevel := flag.Int("logLevel", 0, "Logging level - 0 Error, 1 Warn, 2 Info, 3 Debug, 4 Trace")
	flag.Parse()
	//logger.Log is a global logger that can be used in any package in your app once configured
	globalLogger, err := logger.GetGlobalLogger(logger.Config{
		//Set the level to show, this is usually configure by a flag
		//on app startup
		Level: logger.LogLevel(*logLevel),

		//Application name or "tag"
		Tag: "sbr-test-service",

		//URI for where to send the logs, currently supports three
		//file://{path} (defaults to {tag}.log)
		//fluent://{host:port} (defaults to 127.0.0.1:24224)
		//console://{stdout/stderr} (defaults to stdout)
		//If left blank will default to console
		DestURI: "file://test.log",

		//Allows you to overide default colormap
		//var logColors = map[string]string{
		// 	"Error": "red",
		// 	"Warn":  "yellow",
		// 	"Info":  "none",
		// 	"Debug": "cyan",
		// 	"Trace": "white",
		// }
		ColorMap: map[string]string{
			"Error": "green",
		},
	})

	if err != nil {
		panic(fmt.Sprintf("Cannot load logger: %s", err))
	}

	someValue := "test"
	globalLogger.Info("Test with fomatting %s", someValue)

	errorCallback := func(err error) {
		fmt.Printf("Error from logger: %s\n", err)
	}

	fluentLogger, _ := logger.NewLogger(logger.Config{
		Level: logger.LogDebug,
		Tag:   "sbr-test-server(special stuff)",
		//Easy way to mock fluentd:
		//$> npm install -g ecco
		//$> ecco -l 24224 --address 0.0.0.0
		DestURI:       "fluent://127.0.0.1:24224",
		Async:         true,
		ErrorCallback: errorCallback,
	})

	data := map[string]string{
		"some":   "special",
		"object": "data",
	}

	//Each function (Error, Warn, Info, Debug, and Trace) have
	//a respective O function for logging in the form of a
	//map[string]string with the added keys of "tag" and "timestamp"
	for i := 0; i < 5; i++ {
		fluentLogger.DebugO(data)
		time.Sleep(time.Second)
	}

	fluentLogger.Error("Strings sent to fluentd are put in a dict with key \"log\" ")

	consoleLogger, _ := logger.NewLogger()

	consoleLogger.Error("Default settings send messages to console stdout only showing Error")

	consoleLogger.Warn("Won't show because the default logLevel is Error")

	globalLogger.TraceO(data)
	consoleLogger.ErrorO(data)
	someFunc()
}

func someFunc() {
	globalLogger, _ := logger.GetGlobalLogger()
	globalLogger.Info("Same object again, doesn't need to be reinitiaed")
}
