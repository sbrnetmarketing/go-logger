package logger

import (
	"fmt"
	"strings"

	"strconv"

	"github.com/fluent/fluent-logger-golang/fluent"
)

const defaultHost = "127.0.0.1"
const defaultPort = "24224"

type fluentLogger struct {
	host   string
	port   int
	tag    string
	client *fluent.Fluent
}

func newFluentLogger(hostString string, tag string) (writer WriterInterface, err error) {
	var host, port string
	parsedHostString := strings.Split(hostString, ":")
	switch len(parsedHostString) {
	case 0:
		host = defaultHost
		port = defaultPort
	case 1:
		host = parsedHostString[0]
		port = defaultPort
	case 2:
		host = parsedHostString[0]
		port = parsedHostString[1]
	default:
		err = fmt.Errorf("Cannot parse hostString: %s", hostString)
	}
	intPort, err := strconv.Atoi(port)
	f, err := fluent.New(fluent.Config{FluentHost: host, FluentPort: intPort})
	if err != nil {
		return nil, err
	}

	return &fluentLogger{
		host:   host,
		port:   intPort,
		tag:    tag,
		client: f,
	}, nil
}

func (f *fluentLogger) writeString(s string) (err error) {
	data := map[string]string{
		"log": s,
	}
	err = f.client.Post(f.tag, data)
	return err
}

func (f *fluentLogger) writeObject(m map[string]string) (err error) {
	err = f.client.Post(f.tag, m)
	return err
}

func (f *fluentLogger) supportsColors() bool {
	return false
}
