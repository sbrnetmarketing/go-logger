package logger

import (
	"fmt"
	"net/url"
	"runtime"
	"strings"
	"time"
)

const (
	defaultLevel = LogError
	defaultTag   = "application"
)

var colorFuncs = map[string]interface{}{
	"red":    red,
	"green":  green,
	"yellow": yellow,
	"cyan":   cyan,
	"blue":   cyan,
	"white":  white,
	"none":   none,
}

var logColors = map[string]string{
	"Error": "red",
	"Warn":  "yellow",
	"Info":  "none",
	"Debug": "cyan",
	"Trace": "white",
}

//LogLevel is the level of which messages are displayed
type LogLevel int

//Log levels
const (
	LogError LogLevel = iota
	LogWarn
	LogInfo
	LogDebug
	LogTrace
)

var logLevelsByName = map[string]LogLevel{
	"ERROR": LogError,
	"WARN":  LogWarn,
	"INFO":  LogInfo,
	"DEBUG": LogDebug,
	"TRACE": LogTrace,
}

// LogLevelByName allows levels to be specified by name, e.g. from config files
func LogLevelByName(levelName string) LogLevel {
	var level LogLevel
	var ok bool
	if level, ok = logLevelsByName[strings.ToUpper(levelName)]; !ok {
		return LogError
	}
	return level
}

//WriterInterface is the interface for writing logs
type WriterInterface interface {
	writeString(s string) (err error)
	writeObject(o map[string]string) (err error)
	supportsColors() bool
}

//DestinationType is the type of destination
type DestinationType string

//Log destination types
const (
	LogConsole DestinationType = "console"
	LogFile                    = "file"
	LogFluentd                 = "fluent"
)

type logger struct {
	config   Config
	writer   WriterInterface
	colorMap map[string]string
}

//LogInterface is the interface for the logger object
type LogInterface interface {
	Error(message string, a ...interface{}) (err error)
	ErrorO(message map[string]string) (err error)
	Warn(message string, a ...interface{}) (err error)
	WarnO(message map[string]string) (err error)
	Info(message string, a ...interface{}) (err error)
	InfoO(message map[string]string) (err error)
	Debug(message string, a ...interface{}) (err error)
	DebugO(message map[string]string) (err error)
	Trace(message string, a ...interface{}) (err error)
	TraceO(message map[string]string) (err error)
	GetAsync() bool
	SetAsync(bool)
}

//Config holds the configuration for the logger
type Config struct {
	//LogLevel is the current logging level
	Level LogLevel

	//Tag is used by some logging interfaces
	Tag string

	//DestURI is the destination for the logger in the form of a URI (e.g. file://test.log)
	//If it is a plain string will be read as a filename and if empty treated as stdout
	DestURI string

	//ColorMap is a dict of the function name and the color to use
	// i.e. {"Error": "green"}
	ColorMap map[string]string

	//NoColors if true will turn off colors completely
	NoColors bool

	//Async sets whether or not to make log writes asynchronously
	Async bool

	//ErrorCallback is a function to send errors to during asynchornous operation
	ErrorCallback func(error)
}

//appLog is a shared logger object
var appLog LogInterface

//NewLogger returns a new logger   level LogLevel, destURI string, tag string
func NewLogger(configs ...Config) (LogInterface, error) {
	var config Config
	if len(configs) == 0 {
		config = Config{}
	} else {
		config = configs[0]
	}
	if config.Tag == "" {
		config.Tag = defaultTag
	}
	if config.Level == 0 {
		config.Level = defaultLevel
	}
	if config.Async == true {
		if config.ErrorCallback == nil {
			fmt.Println("Error: Must pass error channel when in async mode")
		}
	}

	destType, destString, err := parseDestinationType(config.DestURI)
	if err != nil {
		fmt.Printf("Error parsing log destination: %s\n", err)
		return nil, err
	}

	writer, err := getWriter(destType, destString, config)
	if err != nil {
		fmt.Printf("Error getting log writer: %s\n", err)
		return nil, err
	}

	l := &logger{config: config, writer: writer}
	l.colorMap = make(map[string]string)

	if !config.NoColors {
		l.colorMap = make(map[string]string)
		for k, v := range logColors {
			l.colorMap[k] = v
		}

		for k, v := range config.ColorMap {
			if _, ok := colorFuncs[v]; ok {
				l.colorMap[k] = v
			}
		}
	} else {
		for k := range logColors {
			l.colorMap[k] = "none"
		}
	}
	return l, nil
}

//GetGlobalLogger returns a pointer to appLog and initializes it if necessary
func GetGlobalLogger(configs ...Config) (LogInterface, error) {
	var err error
	if appLog == nil {
		appLog, err = NewLogger(configs...)
	}
	return appLog, err
}

//ErrorS logs error messages
func (l *logger) Error(message string, a ...interface{}) (err error) {
	if l.config.Level >= LogError {
		if l.writer.supportsColors() {
			message = l.colorize(message)
		}
		err = l.logString(message, a...)
	}
	return err
}

//ErrorO logs error messages as objects
func (l *logger) ErrorO(message map[string]string) (err error) {
	if l.config.Level >= LogError {
		err = l.logObject(message)
	}
	return err
}

//Warn logs warning messages
func (l *logger) Warn(message string, a ...interface{}) (err error) {
	if l.config.Level >= LogWarn {
		if l.writer.supportsColors() {
			message = l.colorize(message)
		}
		err = l.logString(message, a...)
	}
	return err
}

//WarnO logs warn messages as objects
func (l *logger) WarnO(message map[string]string) (err error) {
	if l.config.Level >= LogWarn {
		err = l.logObject(message)
	}
	return err
}

//Info logs informational messages
func (l *logger) Info(message string, a ...interface{}) (err error) {
	if l.config.Level >= LogInfo {
		if l.writer.supportsColors() {
			message = l.colorize(message)
		}
		err = l.logString(message, a...)
	}
	return err
}

//InfoO logs informational messages as objects
func (l *logger) InfoO(message map[string]string) (err error) {
	if l.config.Level >= LogInfo {
		err = l.logObject(message)
	}
	return err
}

//Debug logs debug messages
func (l *logger) Debug(message string, a ...interface{}) (err error) {
	if l.config.Level >= LogDebug {
		if l.writer.supportsColors() {
			message = l.colorize(message)
		}
		err = l.logString(message, a...)
	}
	return err
}

//DebugO logs debug messages as objects
func (l *logger) DebugO(message map[string]string) (err error) {
	if l.config.Level >= LogDebug {
		err = l.logObject(message)
	}
	return err
}

//Trace logs trace messages
func (l *logger) Trace(message string, a ...interface{}) (err error) {
	if l.config.Level >= LogTrace {
		if l.writer.supportsColors() {
			message = l.colorize(message)
		}
		err = l.logString(message, a...)
	}
	return err
}

//TraceO logs trace messages as objects
func (l *logger) TraceO(message map[string]string) (err error) {
	if l.config.Level >= LogTrace {
		err = l.logObject(message)
	}
	return err
}

func (l *logger) GetAsync() bool {
	return l.config.Async
}

func (l *logger) SetAsync(async bool) {
	l.config.Async = async
}

func (l *logger) logString(message string, a ...interface{}) (err error) {
	if l.config.Async {
		go func() {
			err = l.writer.writeString(
				fmt.Sprintf("[%s %v] %s\n", l.config.Tag, time.Now(), fmt.Sprintf(message, a...)))
			if err != nil {
				l.config.ErrorCallback(err)
			}
		}()
	} else {
		err = l.writer.writeString(
			fmt.Sprintf("[%s %v] %s\n", l.config.Tag, time.Now(), fmt.Sprintf(message, a...)))
	}
	return err
}

func (l *logger) logObject(message map[string]string) (err error) {
	message["timestamp"] = fmt.Sprintf("%v", time.Now())
	message["tag"] = l.config.Tag
	if l.config.Async {
		go func() {
			err = l.writer.writeObject(message)
			if err != nil {
				l.config.ErrorCallback(err)
			}
		}()
	} else {
		err = l.writer.writeObject(message)
	}
	return err
}

func (l *logger) colorize(s string) string {
	pc := make([]uintptr, 10)
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	fullName := strings.Split(f.Name(), ".")
	functionName := fullName[len(fullName)-1]
	return colorFuncs[l.colorMap[functionName]].(func(string) string)(s)
}

func red(s string) string {
	return colorString(31, s)
}

func green(s string) string {
	return colorString(32, s)
}

func yellow(s string) string {
	return colorString(33, s)
}

func cyan(s string) string {
	return colorString(36, s)
}

func white(s string) string {
	return colorString(37, s)
}

func none(s string) string {
	return s
}

func colorString(c int, s string) string {
	return fmt.Sprintf("\x1b[%dm\x1b[1m%s\x1b[0m", c, s)
}

func parseDestinationType(destURI string) (destType DestinationType, destString string, err error) {
	parsedDest, err := url.Parse(destURI)
	if err != nil {
		return LogConsole, "", err
	}

	if len(destURI) == 0 || parsedDest.Scheme == "console" {
		destType = LogConsole
	} else if parsedDest.Scheme == "file" || parsedDest.Scheme == "" {
		destType = LogFile
	} else {
		destType = DestinationType(parsedDest.Scheme)
	}

	destString = strings.TrimPrefix(parsedDest.String(), fmt.Sprintf("%s://", parsedDest.Scheme))

	return destType, destString, err
}

func getWriter(destType DestinationType, destString string, config Config) (writer WriterInterface, err error) {
	switch destType {
	case LogConsole:
		writer = newConsoleLogger(consoleOutput(destString))
	case LogFile:
		writer, err = newFileLogger(destString, config.Tag)
	case LogFluentd:
		writer, err = newFluentLogger(destString, config.Tag)
	default:
		err = fmt.Errorf("Unsupported type: %s", destType)
	}
	if err != nil {
		return nil, err
	}
	return writer, nil
}
