package logger

import "os"
import "encoding/json"

type consoleLogger struct {
	writer *os.File
}

type consoleOutput string

const (
	osStdOut consoleOutput = "stdout"
	osStdErr consoleOutput = "stderr"
)

func newConsoleLogger(dest consoleOutput) *consoleLogger {
	var writer *os.File
	switch dest {
	case osStdErr:
		writer = os.Stderr
	case osStdOut:
		fallthrough
	default:
		writer = os.Stdout
	}
	return &consoleLogger{writer: writer}
}

func (c *consoleLogger) writeString(s string) (err error) {
	_, err = c.writer.WriteString(s)
	return err
}

func (c *consoleLogger) writeObject(m map[string]string) error {
	ms, err := json.Marshal(m)
	if err == nil {
		err = c.writeString(string(ms))
	}
	return err
}

func (c *consoleLogger) supportsColors() bool {
	return true
}
