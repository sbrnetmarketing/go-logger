package logger

import (
	"encoding/json"
	"fmt"
	"os"
)

type fileLogger struct {
	writer *os.File
}

func newFileLogger(fileName string, tag string) (*fileLogger, error) {
	if fileName == "" {
		fileName = fmt.Sprintf("%s.log", tag)
	}
	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	return &fileLogger{writer: f}, err
}

func (f *fileLogger) writeString(s string) (err error) {
	_, err = f.writer.WriteString(s)
	return err
}

func (f *fileLogger) writeObject(m map[string]string) error {
	ms, err := json.Marshal(m)
	if err == nil {
		err = f.writeString(string(ms))
	}
	return err
}

func (f *fileLogger) supportsColors() bool {
	return true
}
